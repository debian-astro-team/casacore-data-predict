#!/bin/sh

set -e

if [ $# -gt 0 ] ; then
    type=$1
else
    type=ALL
fi

mkdir -p build/geodetic

cd build

measuresdata type=$1

while grep -qe "^status:\\Wcont" measuresdata.rc ; do
    arg=$(grep "^arg:" measuresdata.rc | cut -d\  -f2-)
    file=$(grep "^file:" measuresdata.rc | cut -d\  -f2-)
    ln -s ../$file .
    measuresdata $arg
done
